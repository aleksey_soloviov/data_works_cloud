#!/bin/sh


VERSION=$1


docker build -t registry.gitlab.com/aleksey_soloviov/data_works_cloud/airflow:$VERSION -t registry.gitlab.com/aleksey_soloviov/data_works_cloud/airflow:latest .
docker push registry.gitlab.com/aleksey_soloviov/data_works_cloud/airflow:$VERSION
docker push registry.gitlab.com/aleksey_soloviov/data_works_cloud/airflow:latest
